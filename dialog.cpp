#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>

QTreeWidgetItem * Dialog::addRoot(QString titulo, QString descripcion)
{
    QTreeWidgetItem * item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0, titulo);
    item->setText(1, descripcion);
    ui->treeWidget->addTopLevelItem(item);

    return item;
}

QTreeWidgetItem * Dialog::addChild(QTreeWidgetItem * parent, QString titulo, QString descripcion)
{
    QTreeWidgetItem * item = new QTreeWidgetItem(parent);
    item->setText(0, titulo);
    item->setText(1, descripcion);
    parent->addChild(item);

    return item;
}

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    ui->treeWidget->setColumnCount(2);
    ui->treeWidget->setHeaderLabels(QStringList({"Titulo", "Descripcion"}));

    QTreeWidgetItem * item = addRoot("1 Elemento Raiz", "Primer raiz del arbol");
    addChild(item, "1er Hijo", "Primer hijo del raiz 1");
    addChild(item, "2do Hijo", "Segundo hijo del raiz 1");

    item = addRoot("2 Elemento Raiz", "Segundo raiz del arbol");
    addChild(item, "1er Hijo", "Primer hijo del raiz 2");
    addChild(item, "2do Hijo", "Segundo hijo del raiz 2");

}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QTreeWidgetItem * item =ui->treeWidget->currentItem();
    item->setForeground(0, Qt::red);
    item->setForeground(1, Qt::blue);
    QMessageBox::information(this, "Titulo", item->text(0) + "<br>" + item->text(1), QMessageBox::Yes);
}
