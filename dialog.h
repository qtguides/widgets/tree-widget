#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Dialog *ui;

    QTreeWidgetItem * addRoot(QString titulo, QString descripcion);
    QTreeWidgetItem * addChild(QTreeWidgetItem * parent, QString titulo, QString descripcion);
};

#endif // DIALOG_H
